/* =======================

- Classify -

made by FV iMAGINATION ©2015
for CodeCanyon

==========================*/


import UIKit

import AudioToolbox


class Home: UIViewController,
UITextFieldDelegate,
UIPickerViewDataSource,
UIPickerViewDelegate

{

    /* Views */
    @IBOutlet var searchOutlet: UIButton!
    @IBOutlet var termsOfUseOutlet: UIButton!
    
    @IBOutlet var fieldsView: UIView!
    @IBOutlet var keywordsTxt: UITextField!
    @IBOutlet var whereTxt: UITextField!
    @IBOutlet var categoryTxt: UITextField!
    
    @IBOutlet var categoryContainer: UIView!
    @IBOutlet var categoryPickerView: UIPickerView!
    
    @IBOutlet var categoriesScrollView: UIScrollView!
    
   

    
    /* Variables */
    var classifArray = NSMutableArray()
    var catButton = UIButton()
    
    

override func viewWillAppear(animated: Bool) {
    searchedAdsArray.removeAllObjects()
}
override func viewDidLoad() {
        super.viewDidLoad()
    
    // Init AdMob interstitial
    let delayTime = dispatch_time(DISPATCH_TIME_NOW,
        Int64(3 * Double(NSEC_PER_SEC)))
   
       
    // Round views corners
    searchOutlet.layer.cornerRadius = 8
    searchOutlet.layer.shadowColor = UIColor.blackColor().CGColor
    searchOutlet.layer.shadowOffset = CGSizeMake(0, 1.5)
    searchOutlet.layer.shadowOpacity = 0.8

    termsOfUseOutlet.layer.cornerRadius = 8
    
    
    // Put fieldsView in the center of the screen
    if UIDevice.currentDevice().userInterfaceIdiom == UIUserInterfaceIdiom.Pad {
        fieldsView.center = CGPointMake(view.frame.size.width/2, 300 )
    }
    
    // Hide the Categ. PickerView
    categoryContainer.frame.origin.y = view.frame.size.height
    view.bringSubviewToFront(categoryContainer)
    
    setupCategoriesScrollView()
    
}

    
// SETUP CATEGORIES SCROLL VIEW
func setupCategoriesScrollView() {
        var xCoord: CGFloat = 5
        let yCoord: CGFloat = 0
        let buttonWidth:CGFloat = 90
        let buttonHeight: CGFloat = 90
        let gapBetweenButtons: CGFloat = 5
        
        var itemCount = 0
        
        // Loop for creating buttons ========
        for itemCount = 0; itemCount < categoriesArray.count; itemCount++ {
            // Create a Button
            catButton = UIButton(type: UIButtonType.Custom)
            catButton.frame = CGRectMake(xCoord, yCoord, buttonWidth, buttonHeight)
            catButton.tag = itemCount
            catButton.showsTouchWhenHighlighted = true
            catButton.setTitle("\(categoriesArray[itemCount])", forState: UIControlState.Normal)
            catButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Thin", size: 12)
            catButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            catButton.setBackgroundImage(UIImage(named: "\(categoriesArray[itemCount])"), forState: UIControlState.Normal)
            catButton.contentVerticalAlignment = UIControlContentVerticalAlignment.Bottom
            catButton.layer.cornerRadius = 5
            catButton.clipsToBounds = true
            catButton.addTarget(self, action: "catButtTapped:", forControlEvents: UIControlEvents.TouchUpInside)
            
            // Add Buttons & Labels based on xCood
            xCoord +=  buttonWidth + gapBetweenButtons
            categoriesScrollView.addSubview(catButton)
        } // END LOOP ================================
    
        // Place Buttons into the ScrollView =====
        categoriesScrollView.contentSize = CGSizeMake( (buttonWidth+5) * CGFloat(itemCount), yCoord)
}


    
    
// CATEGORY BUTTON TAPPED
func catButtTapped(sender: UIButton) {
    let button = sender as UIButton
    let categoryStr = "\(button.titleForState(UIControlState.Normal)!)"
    searchedAdsArray.removeAllObjects()
    
    let query = PFQuery(className: CLASSIF_CLASS_NAME)
    query.whereKey(CLASSIF_CATEGORY, equalTo: categoryStr)
    query.orderByAscending(CLASSIF_UPDATED_AT)
    query.limit = 30
    query.findObjectsInBackgroundWithBlock { (objects, error)-> Void in
        if error == nil {
            if let objects = objects! as? [PFObject] {
                for object in objects {
                    searchedAdsArray.addObject(object)
            } }
            // Go to Browse Ads VC
            let baVC = self.storyboard?.instantiateViewControllerWithIdentifier("BrowseAds") as! BrowseAds
            self.navigationController?.pushViewController(baVC, animated: true)
            
            
        } else {
            let alert = UIAlertView(title: APP_NAME,
            message: "Something went wrong, try again later or check your internet connection",
            delegate: self,
            cancelButtonTitle: "OK" )
            alert.show()
        }
    }

    
}
    
    
    
    
// SEARCH BUTTON
@IBAction func searchButt(sender: AnyObject) {
    searchedAdsArray.removeAllObjects()

    var keywordsArray = keywordsTxt.text!.componentsSeparatedByString(" ") as NSArray

    var query = PFQuery(className: CLASSIF_CLASS_NAME)
    query.whereKey(CLASSIF_DESCRIPTION_LOWERCASE, containsString: "\(keywordsArray[0])")
    query.whereKey(CLASSIF_CATEGORY, equalTo: categoryTxt.text!)
    query.whereKey(CLASSIF_ADDRESS_STRING, containsString: whereTxt.text!.lowercaseString)
    query.orderByAscending(CLASSIF_UPDATED_AT)
    query.limit = 30
    query.findObjectsInBackgroundWithBlock { (objects, error)-> Void in
        if error == nil {
            if let objects = objects! as? [PFObject] {
                for object in objects {
                    searchedAdsArray.addObject(object)
                } }
            if searchedAdsArray.count > 0 {
            // Go to Browse Ads VC
             let baVC = self.storyboard?.instantiateViewControllerWithIdentifier("BrowseAds") as! BrowseAds
            self.navigationController?.pushViewController(baVC, animated: true)
          
            } else {
            var alert = UIAlertView(title: APP_NAME,
            message: "Nothing found with your search keywords, try different keywords, location or category",
            delegate: nil,
            cancelButtonTitle: "OK" )
            alert.show()
            }
            
            
        } else {
            var alert = UIAlertView(title: APP_NAME,
            message: "Something went wrong, try again later or check your internet connection",
            delegate: nil,
            cancelButtonTitle: "OK" )
            alert.show()
        }
    }
    

}

    
    
    
/* MARK -  TEXTFIELD DELEGATE */
func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
    if textField == categoryTxt {
        showCatPickerView()
        keywordsTxt.resignFirstResponder()
        whereTxt.resignFirstResponder()
        categoryTxt.resignFirstResponder()
    }
        
return true
}
    
func textFieldDidBeginEditing(textField: UITextField) {
    if textField == categoryTxt {
        showCatPickerView()
        keywordsTxt.resignFirstResponder()
        whereTxt.resignFirstResponder()
        categoryTxt.resignFirstResponder()
    }
}
    
func textFieldShouldReturn(textField: UITextField) -> Bool {
    if textField == keywordsTxt {  whereTxt.becomeFirstResponder(); hideCatPickerView()  }
    if textField == whereTxt {  categoryTxt.becomeFirstResponder()  }

return true
}
    
    
    
/* MARK - PICKERVIEW DELEGATES */
func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
    return 1;
}

func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
    return categoriesArray.count
}
    
func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
    return categoriesArray[row]
}

func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
    categoryTxt.text = "\(categoriesArray[row])"
}

    
// POST A NEW AD BUTTON
@IBAction func postAdButt(sender: AnyObject) {
    let postVC = self.storyboard?.instantiateViewControllerWithIdentifier("Post") as! Post
    postVC.postID = ""
    presentViewController(postVC, animated: true, completion: nil)
}

    
// PICKERVIEW DONE BUTTON
@IBAction func doneButt(sender: AnyObject) {
        hideCatPickerView()
}
    
    
// DISMISS KEYBOARD ON TAP
@IBAction func dismissKeyboardOnTap(sender: UITapGestureRecognizer) {
    keywordsTxt.resignFirstResponder()
    whereTxt.resignFirstResponder()
    categoryTxt.resignFirstResponder()
    hideCatPickerView()
}
    
    
// SHOW/HIDE CATEGORY PICKERVIEW
func showCatPickerView() {
    UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
        self.categoryContainer.frame.origin.y = self.view.frame.size.height - self.categoryContainer.frame.size.height-44
    }, completion: { (finished: Bool) in  });
}
func hideCatPickerView() {
    UIView.animateWithDuration(0.1, delay: 0.0, options: UIViewAnimationOptions.CurveLinear, animations: {
        self.categoryContainer.frame.origin.y = self.view.frame.size.height
    }, completion: { (finished: Bool) in  });
}
    
    
    
//SHOW TERMS OF USE
@IBAction func termsOfUseButt(sender: AnyObject) {
    let touVC = self.storyboard?.instantiateViewControllerWithIdentifier("TermsOfUse") as! TermsOfUse
    presentViewController(touVC, animated: true, completion: nil)
}
    
    
    
override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
}
}
